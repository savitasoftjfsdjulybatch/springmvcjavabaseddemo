package org.ssce.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/message")
public class MessageController {
	
	
	@GetMapping
	public String helloMessage(Model model) {
		model.addAttribute("msg", "What ever I do I do it well");
		return "hello";
	}

}
