package org.ssce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.ssce.model.Customer;
import org.ssce.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	
	@GetMapping("/addCustomer")
	public String addCustomer(Model model) {
		
		Customer customer = new Customer();
		model.addAttribute(customer);
		
		return "addCustomer";
		
	}
	
	@PostMapping("/addCustomer")
	public String addCustomer(@ModelAttribute("customer") Customer customer, Model model) {
		
		customerService.addCustomer(customer);
		
		List<Customer> list = customerService.getAllCustomers();
		
		model.addAttribute("customers",list);
		return "listCustomers";
	}
	
	
	@GetMapping("/listCustomer")
	public String listCustomer(Model model) {
		
		List<Customer> list = customerService.getAllCustomers();
		
		model.addAttribute("customers",list);
		
		return "listCustomers";
		
	}
	
	@GetMapping("/modifyCustomer")
	public String modifyCustomer(@RequestParam("customerId") Integer customerId,Model model) {
		
		
		model.addAttribute(customerService.getCustomer(customerId));
		return "modifyCustomer";
	}
	
	@PostMapping("/modifyCustomer")
	public String modifyCustomer(@ModelAttribute("customer") Customer customer,Model model) {
		customerService.modifyCustomer(customer);
		
		List<Customer> list = customerService.getAllCustomers();
		model.addAttribute("customers",list);
		return "listCustomers";
	}
	
	@GetMapping("/deleteCustomer")
	public String deleteCustomer(@RequestParam("customerId") Integer customerId,Model model) {
		
		
		model.addAttribute(customerService.getCustomer(customerId));
		return "deleteCustomer";
	}
	
	@PostMapping("/deleteCustomer")
	public String deleteCustomer(@ModelAttribute("customer") Customer customer,Model model) {
		customerService.deleteCustomer(customer);
		
		List<Customer> list = customerService.getAllCustomers();
		model.addAttribute("customers",list);
		return "listCustomers";
	}

}
