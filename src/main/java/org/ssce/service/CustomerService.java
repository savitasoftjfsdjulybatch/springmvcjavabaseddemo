package org.ssce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ssce.model.Customer;
import org.ssce.repository.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	
	public Customer addCustomer(Customer customer) {
		return customerRepository.addCustomer(customer);
	}
	
	public List<Customer> getAllCustomers(){
		return customerRepository.findAll();
	}
	
	public Customer getCustomer(Integer customerId) {
		return customerRepository.find(customerId);
	}
	
	public Customer modifyCustomer(Customer customer) {
		return customerRepository.modifyCustomer(customer);
	}
	
	public Customer deleteCustomer(Customer customer) {
		return customerRepository.deleteCustomer(customer);
	}

}
