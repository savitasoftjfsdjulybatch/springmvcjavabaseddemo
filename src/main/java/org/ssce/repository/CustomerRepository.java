package org.ssce.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.ssce.model.Customer;

@Repository
public class CustomerRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
    public Customer addCustomer(Customer customer) {
    	
    	Session session = sessionFactory.getCurrentSession();
    	session.save(customer);
    	
    	return customer;
    	
    }
	
	@Transactional
	public List<Customer> findAll(){
		
		Session session = sessionFactory.getCurrentSession();
		
		List<Customer> list = (List<Customer>) session.createQuery("FROM Customer").getResultList();
		return list;
		
	}
	
	@Transactional
	public Customer find(Integer customerId) {
		
		Session session = sessionFactory.getCurrentSession();
		
		Customer customer = session.get(Customer.class, customerId);
		
		return customer;
	}
	
	@Transactional
	public Customer modifyCustomer(Customer customer) {
		
		Session session = sessionFactory.getCurrentSession();
		
		Customer cust = session.get(Customer.class, customer.getCustomerId());
		
		cust.setCustomerName(customer.getCustomerName());
		cust.setCustomerAddress(customer.getCustomerAddress());
		cust.setContactNumber(customer.getContactNumber());
		
		session.update(cust);
		return customer;
	}
	
	@Transactional
	public Customer deleteCustomer(Customer customer) {
		
		Session session = sessionFactory.getCurrentSession();
		
		Customer cust = session.get(Customer.class, customer.getCustomerId());
		
		session.remove(cust);
		
		return customer;
	}
	
	
}
