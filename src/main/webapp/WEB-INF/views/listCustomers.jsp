<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<meta charset="ISO-8859-1">
	<title>Customers</title>
	<style>
		.container{
			margin : 20px;
			padding : 20px;
			background-color: antiquewhite;
		}
	</style>
</head>
<body>
	<div class="container">
		<legend>Customer details</legend>
		<button class="btn btn-success" onclick="window.location.href='addCustomer';return false">Add customer</button>
		<table class="table table-striped table-inverse table-responsive">
			<thead class="thead-inverse">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Address</th>
					<th>Contact</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="cust" items="${customers}">
					<tr>
						<td scope="row">${cust.customerId}</td>
						<td>${cust.customerName}</td>
						<td>${cust.customerAddress}</td>
						<td>${cust.contactNumber}</td>
						<td>
							<a type="button" class="btn btn-outline-primary" href="http://localhost:8080/springmvcjavabaseddemo/customer/modifyCustomer?customerId=${cust.customerId}">Modify</a>
						</td>
						<td>
							<a type="button" class="btn btn-outline-primary" href="http://localhost:8080/springmvcjavabaseddemo/customer/deleteCustomer?customerId=${cust.customerId}">Delete</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>