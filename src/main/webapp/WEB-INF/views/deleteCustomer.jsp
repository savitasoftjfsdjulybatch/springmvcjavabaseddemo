<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<meta charset="ISO-8859-1">
	<title>Delete customer</title>
	<style>
		.container {
			width: 600px;
			margin : 20px;
			padding : 20px;
			background-color: antiquewhite;
		}

		legend{
			font-size: 32px;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<div class="container">
		<legend>Delete customer</legend>
		<form:form action="deleteCustomer" modelAttribute="customer" method="post">
			<div class="form-group">
			  <label for="customerId">Customer ID</label>
			  <form:input class="form-control" path="customerId" id="customerId" />
			</div>

			<div class="form-group">
				<label for="customerName">Customer Name</label>
				<form:input class="form-control" path="customerName" id="customerName" />
			</div>

			<div class="form-group">
				<label for="customerAddress">Customer Address</label>
				<form:textarea class="form-control" path="customerAddress" id="customerAddress" rows="5" cols="40" />
			</div>

			<div class="form-group">
				<label for="contactNumber">Contact Number</label>
				<form:input class="form-control" path="contactNumber" id="contactNumber" />
			</div>

			<br>
			<button class="btn btn-primary" type="submit">Delete customer</button>
		</form:form>
	</div>
	

</body>
</html>